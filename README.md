# Orientações
Bem vindo à FULLWISE ACADEMY! 
Este documento tem como objetivo te orientar nas configurações das stacks para poder utilizar os serviços hospedados em seu próprio servidor.

Qualquer dúvida estaremos no servidor exclusivo para alunos no Discord.

Boa diversão!

## Stacks
Aqui estão todas as stacks que já deixamos semi configurado para que apenas faça as alterações pertinentes. Cada stack terá sua complexidade, portanto será necessário entender minimamente as configurações da aplicação para poder realizar a configuração correta.

No geral, dentro de cada stack, terá um guia simples para poder preencer conforme seu ambiente do servidor (IP, dominios, recursos, etc).

## Configurando a Stack


## Suibindo a stack
Checklist para validar a stack antes de realizar o deploy no Portainer:

### Configurações de pastas
☑️ Faça o mapeamento correto dos dados. **Sempre configure os volumes corretamente** para que a aplicação salve os dados em uma pasta externa, **evitando perda de dados** ao atualizar a stack ou reiniciar o servidor.

🚨 **IMPORTANTE** 🚨 \
Não deixe de visitar a documentação oficial caso tenha dúvida sobre alguma configuração.

🚨 **ATENÇÃO** 🚨 \
Cada aplicação terá uma ou um conjunto de pastas a serem mapeados. Caso esteja configurando alguma stack que não esteja aqui, é importante acessar as informações de configuração na documentação oficial para que faça a configuração correta.

```
-------------------------------------
CONFIGURAÇÕES REMOVIDAS POR BREVIDADE
-------------------------------------

volumes:
   - redis_insight_data:/db #PASTA A SER SALVA NO DIRETORIO MAPEADO

-------------------------------------
CONFIGURAÇÕES REMOVIDAS POR BREVIDADE
-------------------------------------

volumes:
  redis_insight_data:
    external: true
    name: redis_insight_data
```

### Configurações de rede

☑️ Sempre mantenha as aplicações na mesma rede do Docker para que as aplicações possam se comunicar corretamente.

```
-------------------------------------
CONFIGURAÇÕES REMOVIDAS POR BREVIDADE
-------------------------------------

networks:
     - academy_network #NOME DA REDE UTILIZADA NAS STACKS ANTERIORES

-------------------------------------
CONFIGURAÇÕES REMOVIDAS POR BREVIDADE
-------------------------------------

networks:
  academy_network:  # NOME DA REDE UTILIZADA NAS STACKS ANTERIORES
    name: academy_network # NOME DA REDE UTILIZADA NAS STACKS ANTERIORES
    external: true 
```

### Possui acesso por subdominio?

☑️ Verifique se o endereço do domínio no traefik está correto 

```
traefik.http.routers.redis_insight.rule=Host(`subdominio.SEUDOMINIO.com.br`
```

☑️ Verifique se o pameamento da porta da aplicação no traefik está correta.

```
Exemplo: traefik.http.services.redis_insight.loadBalancer.server.port=5001
```

☑️ Faça a configuração do subdominio no Cloudflare. \
☑️ Não esqueça de deixar o **PROXY desabilitado**

### Limitando acesso aos recursos da VPS

☑️ Configure o máximo de recurso que o aplicativo pode utilizar. Isto evita que a sua VPS trave por ter deixado as aplicações utilizarem toda a máquina.

```
resources:
    limits:
        cpus: "1" # Define a quantidade de processadores
        memory: 1024M # Define a quantidade de RAM
```

### Resumo

```
☑️ Configurar mapeamento de pastas
☑️ Configurar mapeamento da rede interna
☑️ Configurar subdominios no Cloudflare
☑️ Configurar subdomínio no Traefik
☑️ Configurar mapamento de porta no Traefik
☑️ Configurar limites de recursos da VPS
```

✅ **PRONTO**
Agora que você passou pelo checklist, pode subir a stack no Portainer!